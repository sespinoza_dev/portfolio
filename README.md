# PORTFOLIO

This is my personal portfolio project. You can visit my web page at [sespinoza.me](https://sespinoza.me). <br />
You can follow my development process through this repository's README.md and my
[twitter](https://twitter.com/sespinoza_dev). <br />
Please feel free to [clone it](https://github.com/sespinoza-dev/portfolio) and use it as an example for 
learning [React.js](https://reactjs.org/) or if you feel like making your own portfolio webpage.

## Getting Started

To get this project up and running you'll need to have some dependencies installed first.

### Prerequisites

The first one is Node.js. Make sure you got it installed before continuing. 

If you don't have it installed yet, then I highly suggest to install it using
[asdf](https://github.com/asdf-vm/asdf).  <br />
They have a step by step installation process and if you are like me and you use too
many programming languages and version managers, this is especially suited for your needs.

### Installing

The other dependencies that you will need are *react*, *react-dom* and *react-scripts* which are 
included in the `package.json` so you only need to run `npm install` to get them installed them
along with all the other dependencies of the project.

```console
npm install

```

If you did all of this right at this point you should have all you need to run the project
in your localhost.

## Run the project

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

## Tests

### `npm run test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Deployment

To deploy a project like this you'll need a server, maybe a droplet in 
[Digital Ocean](https://www.digitalocean.com/) it may be a good way to start and from there to 
make some [nginx](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04)
configurations to use it as a reverse proxy. If all of this sounds too strange, then follow me in 
this quest and I hope soon to have more insight to make an article about it. For now this
[freeCodeCamp](https://www.freecodecamp.org/news/i-built-this-now-what-how-to-deploy-a-react-app-on-a-digitalocean-droplet-662de0fe3f48/) article may be
your best friend.

## Built with

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

## Contributing

1. Clone the project (`git@github.com:sespinoza-dev/portfolio.git && cd todo`)
2. Create your feature branch (`git checkout -b feature/my-new-feature`)
3. Commit your changes (`git commit -am "Add some feature"`)
4. Push to the branch (`git push origin feature/my-new-feature`)
5. Create a new Merge Request/Pull request.

## Contributors

- [@sespinoz](https://github.com/sespinoza-dev) - creator and maintainer

## Acknowledgements

- Special thanks to [Hervis Pichardo](https://gitlab.com/hpichardo) who helped me to learn more about React.js.
- Thanks to [Billie Thompson](https://gist.github.com/PurpleBooth) for the [README.md](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2#installing) structure.

## TODO

- [ ] landing page.
  - [x] header menu bar with burger menu.
  - [x] sticky footer.
  - [ ] minimal main page content.
  - [ ] set react router.
  - [ ] summary section.
  - [ ] skills section.
  - [ ] project section. (mock)
  - [ ] article section. (mock)
  - [ ] testimonial section.
  - [ ] photo section.
- [ ] About section.
- [ ] Contact Section.
- [ ] Project Section.
- [ ] Articles Section.

Title: README.
Author: S.Espinoza.
Date:	January 29, 2020
version: 1.0.0