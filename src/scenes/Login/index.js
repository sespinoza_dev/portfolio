import React, { useState } from 'react'
import { Navigate } from 'react-router-dom'
import {
  Wrapper,
  Container,
  Form,
  Field,
  Button,
} from './styles/index.js'
import { notification } from 'antd'
import { login } from '../../services/login'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import { useDispatch } from 'react-redux'

const Login = () => {
  const dispatch = useDispatch()
  const [values, setValues] = useState({ email: '', password: '' })

  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({ message, description })
  }

  const handleChange = e => {
    const { name, value } = e.target
    setValues({
      ...values,
      [name]: value
    })
  }

  const handleSubmit = async e => {
    try {
      e.preventDefault()
      if (!values.email || !values.password) {
        return openNotificationWithIcon('warning', 'Form', 'please make sure you filled all the fields in the form.')
      }
      const user = await login(values)
      dispatch({
        type: 'USER_DATA',
        payload: user
      })
      setValues({ email: '', password: '' })
      openNotificationWithIcon('success', 'Login', 'Successfully logged!')
    } catch (error) {
      setValues({ email: '', password: '' })
      console.error(error)
      openNotificationWithIcon('error', 'Login', 'Authentication Error, please contact the administrator.')
    }
  }

  return localStorage.getItem("user") ? (
    <Navigate to="/" />
  ) : (
    <Wrapper>
      <Container>
        <h1> LOGIN </h1>
        <Form onSubmit={handleSubmit}>
          <Field>
            <UserOutlined />
            <input
              type="text"
              name="email"
              placeholder="email"
              value={values.email}
              onChange={handleChange}
            />
          </Field>
          <Field>
            <LockOutlined />
            <input
              type="password"
              name="password"
              placeholder="password"
              value={values.password}
              onChange={handleChange}
            />
          </Field>
          <Button type="submit">LOGIN</Button>
        </Form>
      </Container>
  </Wrapper>
  )
}

export default Login
