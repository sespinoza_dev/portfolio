import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  margin-top: 10px;
`

export const Container = styled.div`
  display: flex;
  flex-flow: column ;
  align-items: center;
  min-width: 300px;
  padding: 20px;
  margin: 20px 20px;
  border-radius: 10px;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);

  h1 {
    color: rgba(0,0,0,.85);
    opacity: 0.6;
    font-weight: bold;
  }
`

export const Form = styled.form``

export const Field = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  margin: 10px 0px 25px 0px;
  padding: 5px;
  width: 100%;

  .anticon {
    margin-left: 10px;
  }

  input {
    margin: 5px;
    padding: 5px;
    width: 200px;
    border-top-style: hidden;
    border-right-style: hidden;
    border-left-style: hidden;

    outline: none;
  }
`

export const Button = styled.button`
  border: none;
  border-radius: 5px;
  padding: 5px;
  width: 100%;
  height: 40px;
  opacity: 0.8;
  font-weight: bold;
  font-size: 20px;
  color: #fff;
  background-color: #4c956c;

  :hover {
    opacity: 0.7;
  }
`
