import React from 'react';
import * as moment from 'moment'
import { Link } from "react-router-dom"
import {
  MainArticlesWrapper,
  Item,
  Author,
  PublicationDate,
  Category,
  MainArticleContainer,
  Aside
} from './styles/index.js'

const MainArticles = ({ first, second, third }) => {
  const formatDate = date => moment(date).format('MMM DD, YYYY')
  return(
    <MainArticlesWrapper>
      <MainArticleContainer>
        <Category>{first.category}</Category>
        <h1><Link to={`/articles/${first._id}`}>{first.title}</Link></h1>
        <div>{first.lead}</div>
        <Author>{first.author} ∙ {second.stats.text}</Author>
        <PublicationDate>{formatDate(first.published_at)}</PublicationDate>
      </MainArticleContainer>

      <Aside>
        <Item>
          <Category>{second.category}</Category>
          <h3><Link to={`/articles/${second._id}`}>{second.title}</Link></h3>
          <div>{second.lead}</div>
          <Author>{second.author} ∙ {second.stats.text}</Author>
          <PublicationDate>{formatDate(second.published_at)}</PublicationDate>
        </Item>

        <Item>
          <Category>{third.category}</Category>
          <h3><Link to={`/articles/${third._id}`}>{third.title}</Link></h3>
          <div>{third.lead}</div>
          <Author>{third.author} ∙ {second.stats.text}</Author>
          <PublicationDate>{formatDate(third.published_at)}</PublicationDate>
        </Item>
      </Aside>
    </MainArticlesWrapper>
  )
}
export {
  MainArticles
}