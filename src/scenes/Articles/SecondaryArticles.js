import React from 'react';
import * as moment from 'moment'
import { Link } from "react-router-dom"
import {
  Item,
  Author,
  PublicationDate,
  Category
} from './styles/index.js'


const SecondaryArticles = ({ articles }) => {
  const formatDate = date => moment(date).format('MMM DD, YYYY')
  return (
    <div>
      {
        articles.map(article => <div key={article._id}>
            <Item>
              <Category>{article.category}</Category>
              <h3><Link to={`/articles/${article._id}`}>{article.title}</Link></h3>
              <div>{article.lead}</div>
              <Author>{article.author} ∙ {article.stats.text}</Author>
              <PublicationDate>{formatDate(article.published_at)}</PublicationDate>
            </Item>
          </div>
        )
      }
    </div>
  )
}

export {
  SecondaryArticles
}