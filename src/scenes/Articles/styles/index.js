import styled from 'styled-components'

export const Stripe = styled.div`
  height: 50px;
  opacity: 30%;
  background-color: #def2c8;
`

export const Title = styled.h2`  
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  opacity: 0.6;
  font-size: 30px;
  margin-top: 20px;
`

export const ArticlesWrapper = styled.div`
  margin: auto;
  max-width: 1000px;
`

export const MainArticlesWrapper = styled.div`
  display: flex;  
  flex-flow: row wrap;
`

export const MainArticleContainer = styled.div`
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  flex: 1 0px;
  border-radius: 10px;
  padding: 10px;
  padding-left: 20px;
  border-left: 8px solid #bcd0c7;
  margin: 10px;

  text-decoration: none;
  color: black;
  

  h3 {
    margin-top: 5px;
    margin-bottom: 5px;
  }
  
  h1 {
    margin-top: 10px;
    margin-bottom: 10px;
  }

  a {
   text-decoration: none;
   color: black;
  }

  a:hover {
    color: #91A6FF;
  }
`

export const Item = styled.div`
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  border-radius: 10px;
  padding: 10px;
  padding-left: 20px;
  border-left: 8px solid #bcd0c7;
  margin: 10px;
  text-decoration: none;
  color: black;
  width: 95%;

  h3 {
    margin-top: 5px;
    margin-bottom: 5px;
  }
  
  h1 {
    margin-top: 10px;
    margin-bottom: 10px;
  }

  a {
   text-decoration: none;
   color: black;
  }

  a:hover {
    color: #91A6FF;
  }
`

export const Author = styled.div`
  padding-top: 10px;
  font-size: 12px;
`

export const PublicationDate = styled.div`
  font-size: '12px';
  font-weight: bold;
`

export const Category = styled.div`
  text-transform: uppercase;
  color: #009688;
  font-size: 12px;
`

export const DividerActive = styled.div`
  border: white;

  @media all and (min-width: 800px) {
    border: 3px solid #F2F2F2; margin: 30px;
  }
`

export const Aside = styled.div`
  width: 100%;
  
  @media all and (min-width: 600px) {
    flex: 1 0 0;
  }

  @media all and (min-width: 800px) {
    order: 3;
  }
`