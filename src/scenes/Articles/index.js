import React, { useEffect, useState } from 'react'
import { fetchArticles } from '../../services/articles'
import { MainArticles } from './MainArticles.js'
import { SecondaryArticles } from './SecondaryArticles.js'
import { CoffeeOutlined } from '@ant-design/icons'
import {
  Title,
  ArticlesWrapper,
  DividerActive,
  Stripe
} from './styles'

const Articles = () => {
  const [articles, setArticles] = useState([])
  useEffect(() => {
    const fetchData = async () => {
      try {
        const articleRequest =  await fetchArticles()
        setArticles(articleRequest)
      } catch (error) {
        console.error('not found', error.message)
      }
    }
    fetchData()
  },[])

  const content = articles.length >=3 ?
    (<div>
      <MainArticles first={articles[0]} second={articles[1]} third={articles[2]}/>
      <DividerActive />
      <SecondaryArticles articles={articles.slice(3, articles.lenght)}/>
    </div>) :
    <SecondaryArticles articles={articles}/>

  return (
    <div>
      <Stripe/>
      <Title>
        <CoffeeOutlined />
        Articles
      </Title>
      <ArticlesWrapper>
        {content}
      </ArticlesWrapper>
    </div>
  )
}


export default Articles
