import styled from 'styled-components'

export const Wrapper = styled.div`
  font-family: 'Times New Roman', Times, serif;
  font-size: 20px;
  margin: 30px;
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
`

export const Container = styled.div`
  min-width: 300px;
  color: rgba(0,0,0,.85);
  h1 { opacity: 0.6; };
  h2 { opacity: 0.6; };
  h3 { opacity: 0.6; };

  @media screen and (max-width: 768px){
    line-height: 170%;
    font-size: 16px;
  }
`

export const ContactForm = styled.form``

export const Input = styled.input`
  border: none;
  width: 100%;
  border-radius: 5px;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  padding: 5px;
`

export const Content = styled.textarea`
  height: 100px;
  width: 100%;
  border: none;
  border-radius: 5px;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  padding: 5px;
`

export const Submit = styled.input`
width: 100%;
  font-family: Roboto;
  font-weight: bold;
  font-size: 20px;
  border: none;
  padding: 10px;
  border-radius: 5px;
  opacity: 0.8;
  color: #fff;
  background-color: #4c956c;

  :hover {
    opacity: 0.7;
  }
`

export const FieldContainer = styled.div`
  padding: 10px 0 10px 0;

  strong {
    opacity: 0.7;
  }
`
