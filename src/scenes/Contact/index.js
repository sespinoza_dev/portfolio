import React from 'react'
import { useForm } from 'react-hook-form'
import {
  Wrapper,
  Container,
  Input,
  Content,
  ContactForm,
  FieldContainer,
  Submit,
} from './styles/index.js'
import { notification } from 'antd'
import { sendContactForm } from '../../services/contact/index'

const Contact = () => {
  const { register, handleSubmit } = useForm()

  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
      message,
      description,
    });
  };

  const onSubmit = async data => {
    try {
      if (!data.name || !data.sender || !data.message) {
        return openNotificationWithIcon('warning', 'Form', 'please make sure you fill all the fields in the form.')
      }
      await sendContactForm(data)
      return openNotificationWithIcon('success', 'Form', 'Form sent!')
    } catch (error) {
      return openNotificationWithIcon('error', 'Form', `This is embarrassing, there was an error, please send me a direct email to espinoza.jimenez@gmail.com`)
    }
  }

  return (
    <Wrapper>
      <Container>
        <h1>
          Contact me!
        </h1>
        <h2>
          Got a question or want to hire me? I'll be happy to hear from you.
        </h2>
        <h3>
          Please tell me about your question or project and I'll contact you as soon as possible.
        </h3>
        <ContactForm onSubmit={handleSubmit(onSubmit)}>
          <FieldContainer>
            <strong>
              Name*
            </strong>
            <Input type="text" placeholder="" name="name" ref={register()} />
          </FieldContainer>
          <FieldContainer>
            <strong>
              Email*
            </strong>
            <Input type="text" placeholder="" name="sender" ref={register()} />
          </FieldContainer>
          <FieldContainer>
            <strong>
              How can I help you?*
            </strong>
            <Content name="message" ref={register()} ></Content>
          </FieldContainer>
          <Submit type="submit" value="SEND"/>
        </ContactForm>
      </Container>
    </Wrapper>
  )
}


export default Contact
