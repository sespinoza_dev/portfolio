import styled from 'styled-components'

export const TableWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 20px;
`

export const TableContainer = styled.div`
  width: 90%;
  margin: 20px;

  @media all and (max-width: 800px) {
    width: 100%;
  }
`

export const ActionWrapper = styled.span`
  span {
    padding: 5px;
  }
`