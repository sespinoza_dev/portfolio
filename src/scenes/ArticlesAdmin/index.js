import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import {
  fetchAuthorArticles,
  postArticle,
  deleteArticle,
  publishArticle,
} from '../../services/articles'
import {
  TableWrapper,
  TableContainer,
  ActionWrapper,
} from './styles/index.js'
import {
  DeleteOutlined,
  EditOutlined,
  GlobalOutlined,
} from '@ant-design/icons'
import {
  Table,
  Tag,
  Button,
  Modal,
  Input,
  notification
} from 'antd'

const ArticlesAdmin = () => {
  const [visible, setvisible] = useState(false)
  const [deleteVisible, setDeleteVisible] = useState(false)
  const [deleteArticleTarget, setDeleteArticleTarget] = useState({})
  const userData = useSelector(state => state.user)
  const author = userData.user && userData.user.name
  const authorId = userData.user && userData.user._id
  const [myArticles, setMyArtciles] = useState([])
  const [article, setArticle] = useState({
    author,
    authorId,
    title: '',
    subTitle: '',
    abstract: null,
    category: 'development',
    keywords: [],
    content: ''
  })

  useEffect(() => {
    const fetchData = async () => {
      try {
        const articleRequest =  await fetchAuthorArticles(authorId, userData.token)
        const withkey = articleRequest.map(article => ({ ...article, key: article._id }))
        setMyArtciles(withkey)
      } catch (error) {
        console.error('not found', error.message)
      }
    }
    fetchData()
  },[authorId, userData.token])

  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({ message, description })
  }

  // Edit
  const handleEditAction = (record) => {
    console.log('edit')
    console.log(record)
  }

  // Delete
  const handleOkDelete = async () => {
    try {
      setDeleteVisible(false)
      const acticleDeleted = await deleteArticle(deleteArticleTarget._id, userData.token)
      setMyArtciles(myArticles.filter(article => article._id !== acticleDeleted._id))
      setDeleteArticleTarget({})
      openNotificationWithIcon('success', 'Deleting Article', 'Article deleted')
    } catch (error) {
      console.error(error)
      openNotificationWithIcon('error', 'Deleting Article', 'An error happened, the article was not deleted')
    }
  }

  const handleDeleteAction = record => {
    console.log('Deleting...')
    console.log(record)
    setDeleteVisible(true)
    setDeleteArticleTarget(record)
  }

  // Publish
  const handlePublishAction = async (record) => {
    try {
      console.log('Publish')
      console.log(record)
      const acticlePublished = await publishArticle(record._id, userData.token)
      const withKey = { key: acticlePublished._id, ...acticlePublished }
      const filterAndAddNewStatus = [withKey, ...myArticles.filter(article => article._id !== acticlePublished._id)]
      setMyArtciles(filterAndAddNewStatus)
      openNotificationWithIcon('success', 'Published', 'Article published')
    } catch (error) {
      console.error(error)
      openNotificationWithIcon('error', 'Published', 'An error happened, the article was not published')
    }
  }

  const columns = [
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
      render: (text, record) => <NavLink to={`/articles/${record._id}`}>{text}</NavLink>
    },
    {
      title: 'Status',
      dataIndex: 'published_at',
      key: 'published_at',
      render: text => <p>{text}</p>,
      responsive: ['md']
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
      responsive: ['lg']
    },
    {
      title: 'Keywords',
      key: 'keywords',
      dataIndex: 'keywords',
      responsive: ['md'],
      render: keywords => (
        <>
          {keywords.map(tag => {
            let color = tag.length > 5 ? 'geekblue' : 'green';
            if (tag === 'loser') {
              color = 'volcano';
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <ActionWrapper>
          <NavLink to={`/articles/edit/${record._id}`}>
            <EditOutlined
              title="Edit"
              style={{ cursor: "pointer" }}
              onClick={() => handleEditAction(record)}
            />
          </NavLink>
          <DeleteOutlined
            title="Delete"
            style={{ color:"#eb2f96", cursor: "pointer" }}
            onClick={() => handleDeleteAction(record)}
          />
          <GlobalOutlined
            title="Publish"
            style={{ color: "#52c41a", cursor: "pointer" }}
            onClick={() => handlePublishAction(record)}
          />
        </ActionWrapper>
      ),
    },
  ];

  const handleChange = e => {
    const name = e.target.name
    const value = e.target.value
    if (name ==='keywords') {
      setArticle({
        ...article,
        keywords: value.split(' ')
      })
    } else {
      setArticle({
        ...article,
        [name]: value
      })
    }
  }

  const handleNewArticle = () => {
    setvisible(true)
  }

  const handleOk = async () => {
    try {
      setvisible(false)
      console.log('create article')
      console.log(article)
      const newArticle = await postArticle(article, userData.token)
      openNotificationWithIcon('success', 'Create Article', 'Article created')
      setMyArtciles([{ ...newArticle, key: newArticle._id }, ...myArticles])
    } catch (error) {
      console.error(error)
      openNotificationWithIcon('error', 'Create Article', 'An error happened, the article was not created')
    }
  }

  return (
    <div>
      <TableWrapper>
        <h2> Articles </h2>
        <Button type="primary" onClick={handleNewArticle}>New Artcle</Button>
        <TableContainer>
          <Table columns={columns} dataSource={myArticles} />
        </TableContainer>
      </TableWrapper>
      <Modal
        title="Create new Article"
        visible={visible}
        okText="Create new Article"
        onOk={handleOk}
        onCancel={() => setvisible(false)}
      >
        <Input name="title" placeholder="Title" onChange={handleChange} />
        <Input name="subTitle" placeholder="Subtitle" onChange={handleChange} />
        <Input name="keywords" placeholder="keywords" onChange={handleChange} />
      </Modal>
      <Modal
        title="Delete Article"
        okType="danger"
        visible={deleteVisible}
        onOk={handleOkDelete}
        onCancel={() => setDeleteVisible(false)}
        okText="DELETE"
      >
        <p>Are you sure you want to delete this article?</p>
        <p>Title: <strong>{deleteArticleTarget && deleteArticleTarget.title}</strong></p>
      </Modal>
    </div>
  )
}


export default ArticlesAdmin
