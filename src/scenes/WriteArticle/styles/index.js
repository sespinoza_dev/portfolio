import styled from 'styled-components'

export const WriteArticleWrapper = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  margin-top: 20px;
`

export const WriteArticleContainer = styled.div`
  padding: 20px;
  margin: 20px 20px;
  max-width: 45%;
  width: 100%;
  border-radius: 10px;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);

  @media all and (max-width: 900px) {
    max-width: 100%;
  }
`

export const ContentPreview = styled.div`
  font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
  pre {
    background: #f4f4f4;
    border: 1px solid #ddd;
    border-left: 3px solid #a3a776;
    color: #666;
    page-break-inside: avoid;
    font-family: monospace;
    font-size: 15px;
    line-height: 1.6;
    margin-bottom: 1.6em;
    max-width: 100%;
    overflow: auto;
    padding: 1em 1.5em;
    display: block;
    word-wrap: break-word;
  }

  code {
    background: #f4f4f4;
  }
`

export const EditingContainer = styled.div`
  .ant-input {
    margin-top: 5px;
    margin-bottom: 5px;
  }
  .ant-select-selector {
    margin-top: 5px;
    margin-bottom: 5px;
  }
`

export const EditingArea = styled.div`
  textarea {
    margin: 5px;
    margin-top: 20px;
    font-size: 15px;
    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
    outline: none;
    border: none;
    resize: none;
    width: 100%;
  }
`


export const SaveContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 5px;
`