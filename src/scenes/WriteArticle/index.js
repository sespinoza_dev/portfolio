import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import parse from 'html-react-parser';
import TextareaAutosize from 'react-textarea-autosize'
import { Tabs, Select, Input, notification, Button } from 'antd'
import { toHTML } from '../../services/formater/toHtml'
import { fetchArticle, updateArticle } from '../../services/articles'
import {
  WriteArticleWrapper,
  WriteArticleContainer,
  ContentPreview,
  EditingContainer,
  EditingArea,
  SaveContainer } from './styles/index.js'

const { TabPane } = Tabs;
const { Option } = Select;

const WriteArticle = ({ match }) => {
  const userData = useSelector(state => state.user)
  const [article, setArticle] = useState({
    author: '',
    authorId: '',
    title: '',
    subTitle: '',
    category: 'development',
    keywords: [],
    content: ''
  })
  useEffect(() => {
    const fetchData = async () => {
      try {
        const articleRequest =  await fetchArticle(match.url.split('/')[3])
        setArticle(articleRequest)
      } catch (error) {
        console.error('something bad happened', error);
      }
    }
    fetchData()
  },[match.url])

  const [htmlContent, setHtmlContent] = useState('')

  const handleSwitch = (key) => {
    const headings = `#${article.title}\n## ${article.subTitle}\n**${article.keywords.join(', ')}**`
    const html = parse(toHTML(`${headings} \n${article.content}`))
    setHtmlContent(html)
  }

  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({ message, description })
  }

  const handleChange = e => {
    const name = e.target.name
    const value = e.target.value
    if (name ==='keywords') {
      setArticle({
        ...article,
        keywords: value.split(' ')
      })
    } else {
      setArticle({
        ...article,
        [name]: value
      })
    }
  }

  const handleKeyDown = e => {
    const charCode = String.fromCharCode(e.which).toLowerCase();
    if(e.ctrlKey && charCode === 's') {
      e.preventDefault();
      handleSave()
    }

    // For MAC
    if(e.metaKey && charCode === 's') {
      e.preventDefault();
      handleSave()
    }
  }

  const handleCategoryChange = category => {
    setArticle({ ...article, category })
  }

  const handleSave = async () => {
    try {
      console.log('save article')
      const name = userData.user && userData.user.name
      const authorId = userData.user && userData.user._id
      setArticle({ ...article, author: name, authorId })
      console.log(article)
      const token = userData.token
      if (!token) throw Error('User not logged in!')
      if (!article.title) throw Error('Please put an Title');
      if (!article.subTitle) throw Error('Please put a Subtitle');
      if (!article.category) throw Error('Please choose a Category');
      if (!article.keywords) throw Error('Please provide keywords');
      if (!article.content) throw Error('Please write some content');
      await updateArticle(article, token)
      openNotificationWithIcon('success', 'Article', 'Article saved!')
    } catch (error) {
      console.error(error)
      openNotificationWithIcon('warning', 'Article', error.message)
    }
  }

  return(
    <WriteArticleWrapper>
      <WriteArticleContainer>
        <Tabs defaultActiveKey="Write" onChange={handleSwitch}>
          <TabPane tab="Write" key="Write">
            <EditingContainer>
              <Input name="title" placeholder="Title" value={article.title} onChange={handleChange} />
              <Input name="subTitle" placeholder="Subtitle" value={article.subTitle} onChange={handleChange} />
              <Input name="keywords" placeholder="Keywords" value={article.keywords.join(' ')} onChange={handleChange} />
              <Select name="category" defaultValue="development" style={{ width: 150 }} onChange={handleCategoryChange}>
                <Option value="development">development</Option>
                <Option value="management">management</Option>
                <Option value="devOps">devOps</Option>
                <Option value="random">random</Option>
              </Select>
              <EditingArea>
                <TextareaAutosize
                  name="content"
                  value={article.content}
                  onChange={handleChange}
                  onKeyDown={handleKeyDown}
                  rows="10"
                  cols="30"
                />
              </EditingArea>
            </EditingContainer>
          </TabPane>
          <TabPane tab="Preview" key="Preview">
            <ContentPreview>
              {htmlContent}
            </ContentPreview>
          </TabPane>
        </Tabs>
        <SaveContainer>
          <Button ghost={true} type="primary" shape="round" size="large" onClick={handleSave}>
            Save
          </Button>
        </SaveContainer>
      </WriteArticleContainer>
    </WriteArticleWrapper>
  )
}

export default WriteArticle
