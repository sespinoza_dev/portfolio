import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font: 16px Helvetica, Sans-Serif;
  line-height: 24px;
`

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 900px;
  margin: 20px;

  .anticon {
    margin: 0 10px 0 10px;
  }
`

export const ProfileWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  flex-direction: row-reverse;
  flex-wrap: wrap;
  border-radius: 5px;
  width: 90%;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  margin: 20px 20px;
`

export const Foto = styled.img`
  max-width: 250px;
  max-height: 250px;
  border-radius: 5%;
  margin: 10px;
`

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  line-height: 1;
  opacity: 0.6;
  padding: 10px;
`

export const Email = styled.a`
  color: black;
  text-decoration: none;
  border-bottom: 1px dotted #999;

  :hover {
    border-bottom-style: solid;
    color: black;
  }
`

export const SocialMediaIcon = styled.a`
  color: rgba(134,121,102, 1);

  :hover {
    color: rgba(134,121,102, 1);
  }
`

export const Introduction = styled.div`
  padding: 20px;
  line-height: 26px;
`

export const Section = styled.div`
  align-self: flex-start;
  padding: 20px;
  line-height: 26px;
`

export const SkillList = styled.ul`
  display: flex;
  flex-wrap: wrap;
`

export const SkillItem = styled.li`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
  width: 100px;
  height: 24px;
  margin: 5px;
  list-style-type: none;
  color: #fff;
  font-weight: bold;
  background-color: rgba(134,121,102, 0.8);
`

export const Hr = styled.hr`
  width: 90%;
  margin: 10px;
  opacity: 60%;
`

export const CvLink = styled.a`
  width: 90%;
  font-family: Roboto;
  font-weight: bold;
  font-size: 20px;
  border: none;
  padding: 10px;
  border-radius: 5px;
  opacity: 0.8;
  color: #fff;
  background-color: #4c956c;
  text-align: center;

  :hover {
    opacity: 0.7;
  }

  &:hover {
    color: #fff;
  }
`
