import React from 'react'
import {
  Wrapper,
  Container,
  ProfileWrapper,
  Foto,
  Info,
  Email,
  SocialMediaIcon,
  Introduction,
  Section,
  SkillList,
  SkillItem,
  Hr,
  CvLink,
} from './styles/index.js'
import 'antd/dist/reset.css'
import {
  TwitterOutlined,
  MediumOutlined,
  GithubOutlined,
  LinkedinOutlined,
  BankOutlined,
  FlagOutlined,
  CodeOutlined,
  CoffeeOutlined,
  SolutionOutlined,
} from '@ant-design/icons'
import profileImg from './img/profile.png'

const About = () => {
  return (
    <Wrapper>
      <Container>
        <ProfileWrapper>
          <Foto src={profileImg} alt="profile" />
          <Info>
            <h1>Samuel Espinoza</h1>
            <h2>Software Developer</h2>
            <div>
              <p>
                Cell: <span className="tel">+372 5814 6809</span><br />
              </p>
              <p>
                  Email: <Email href="mailto:espinoza.jimenez@gmail.com">espinoza.jimenez@gmail.com</Email>
              </p>
              <SocialMediaIcon target="_blank" rel="noopener noreferrer twitter" href="https://twitter.com/sespinoza_dev"><TwitterOutlined /></SocialMediaIcon>
              <SocialMediaIcon target="_blank" rel="noopener noreferrer medium" href="https://medium.com/@sespinoza-dev"><MediumOutlined /></SocialMediaIcon>
              <SocialMediaIcon target="_blank" rel="noopener noreferrer github" href="https://github.com/sespinoza-dev"><GithubOutlined /></SocialMediaIcon>
              <SocialMediaIcon target="_blank" rel="noopener noreferrer linkedin" href="https://www.linkedin.com/in/espinoza-jimenez/"><LinkedinOutlined /></SocialMediaIcon>
            </div>
          </Info>
        </ProfileWrapper>

        <CvLink target="_blank" rel="noopener noreferrer sespinoza" href="https://sespinoza.me/files/samuel_espinoza_cv.pdf">Download CV</CvLink>

        <Introduction>
          <p>
            I’m a Software Developer with more than eight years of experience developing projects with <strong>Javascript</strong> using <strong>Node.js, React</strong>, and some <strong>Ruby</strong> and <strong>Crystal</strong> occasional scripts. Designing <strong>APIs</strong> and building <strong>microservices</strong> is my passion and I’m used to deploy them using <strong>Docker</strong>.
          </p>
          <p>
            I enjoy iterating with agile methodologies like Scrum and I like to get involved in projects from start to finish: From the requirements gathering, software architecture design, prototyping, testing and deploying. I’m considered to be a very motivated, proactive and capable person as I’m an insatiable learner to deliver high-quality solutions.
          </p>
        </Introduction>

        <Hr />

        <Section>
          <h3><FlagOutlined />Experience</h3>
          <h4>Full Stack Developer at Games Global Estonia November 2021 - July 2024</h4>
          <ul>
            <li>Author and maintainer of Express API Microservices.</li>
            <li>Maintainer and author of new features of our Internal tools Web page in React.js (Real Dealer Studios).</li>
            <li>Maintainer of C#/.Net applications</li>
            <li>In charge of migrating the current architecture to Docker containers and to the cloud (Azure).</li>
          </ul>
          <h4>Full Stack Developer at Jooycar March 2021 - October 2021</h4>
          <ul>
            <li>Maintainer of Node.js microservices using Express, Ramda, and other libraries for RESTful APIs. Asynchronous Design. </li>
            <li>Deployment with Docker,AWS S3 and lambda functions.</li>
            <li>TDD with Mocha, Chai, Sinon among others.</li>
            <li>Documentation using Swagger.</li>
          </ul>
          <h4>Software developer & DevOps <span> Panther Management SpA. 2017-2021</span></h4>
          <ul>
            <li>Maintainer of the main company service built with Node.js using Express, Redux, Socket.IO, Ramda, Axios. Test: Mocha, Chai, Sinon among others. Deployed with Docker. The app is a real time application running on attention ticket machines that interact with displays and servers.</li>
            <li>Author and maintainer of internal microservices. They are built on Node.js, Crystal and Ruby. Most of them are client integrations using REST or Soap.</li>
            <li>Co-designer of zq a command-line interface built in Crystal, to provide support for the technical support department.</li>
            <li>Maintainer of the creation of Docker images enabling much faster deployment of product updates to clients.</li>
            <li>Designer and maintainer of process automation shell scripts.</li>
          </ul>

          <h4>Software Developer <span>IT Department, UTFSM - 2015</span></h4>
          <ul>
            <li>Participated in the upgrade of the computer department’s Intranet platform of the Universidad Técnica Federíco Santa María using many Ruby Gems.</li>
          </ul>
        </Section>

        <Hr />

        <Section>
          <h3><BankOutlined />Education</h3>
          <p><strong>Ingeniería en Ejecución Informatica (Software Engineering)</strong></p>
          <p>Universidad Técnica Federíco Santa María - Chile</p>

          <h4>Milestones</h4>
          <ul>
            <li>Designer of PESS, Post-Emergency Support System, a software for the management of volunteer crews on the occasion of the Valparaíso fire, April 12, 2014. I led a team of 5 people in the development of PESS. </li>
            <li>Participated in the XXII Software Fair at the Universidad Técnica Federíco Santa María, November 2014.</li>
          </ul>
        </Section>

        <Hr />

        <Section>
          <h3><CodeOutlined /> Skills</h3>
          <h4>Languages</h4>
          <SkillList>
            <SkillItem> Javascript </SkillItem>
            <SkillItem> Bash </SkillItem>
            <SkillItem> Crystal </SkillItem>
            <SkillItem> Ruby </SkillItem>
          </SkillList>
          <h4>Frameworks & Libraries</h4>
          <SkillList>
            <SkillItem> NodeJS </SkillItem>
            <SkillItem> Express </SkillItem>
            <SkillItem> React </SkillItem>
            <SkillItem> Redux </SkillItem>
            <SkillItem> Axios </SkillItem>
            <SkillItem> Moment.js </SkillItem>
            <SkillItem> Socket.IO </SkillItem>
            <SkillItem> Kemal </SkillItem>
            <SkillItem> Ramda </SkillItem>
          </SkillList>
          <h4>Testing and Development Libraries</h4>
          <SkillList>
            <SkillItem> Mocha </SkillItem>
            <SkillItem> Rewire </SkillItem>
            <SkillItem> Chai </SkillItem>
            <SkillItem> Nodemon </SkillItem>
            <SkillItem> Sinon </SkillItem>
            <SkillItem> ESlint </SkillItem>
            <SkillItem> Git </SkillItem>
            <SkillItem> Redis </SkillItem>
          </SkillList>
          <h4>Computer Software</h4>
          <SkillList>
            <SkillItem> GNU/Linux </SkillItem>
            <SkillItem> Docker </SkillItem>
          </SkillList>
        </Section>

        <Hr />

        <Section>
          <h3><CoffeeOutlined />Hobbies</h3>
          <p>Classical Music, Pianist, Photography, Trekking</p>
        </Section>

        <Hr />

        <Section>
          <h3><SolutionOutlined />References</h3>
          <p>
            Ernesto Erdmann, CEO Panther Management SpA, <a className="email" href="mailto:eerdmann@zeroq.cl">eerdmann@zeroq.cl</a>
          </p>
          <p>
            Daniel Salazar, Technical Leader at Jooycar and former CTO of Panther Management SpA, <a className="email" href="mailto:ddisalazarg@gmail.com">disalazarg@gmail.com</a>
          </p>
        </Section>
      </Container>
    </Wrapper>
  )
}

export default About
