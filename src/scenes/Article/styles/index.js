import styled from 'styled-components'

export const Stripe = styled.div`
  height: 100px;
  /* opacity: 30%; */
  opacity: ${({ theme }) => theme.toggleBorderOpacity};
  /* background-color: #def2c8; */
  background-color: ${({ theme }) => theme.toggleBorder};
`

export const ArticleWrapper = styled.div`
  background-color: ${({ theme }) => theme.body};
  color: ${({ theme }) => theme.text};
  display: flex;
  justify-content: center;

  font-family: 'Merriweather', serif;
  font-size: 17px;
  word-spacing: 3px;
`

export const ArticleContainer = styled.div`
  width: 37%;

  .ant-switch {
    background-color: #a3a776;
  }

  @media all and (max-width: 800px) {
    width: 100%;
    max-width: 90%;
  }
`

export const Lead = styled.div`
  padding: 10px 0px 5px 0px;
  color: #7D7D7D;
`

export const Title = styled.div`
  padding: 40px 0px 10px 0px;
  line-height: 25px;
  font-size: 30px;
  font-family: 'arial';
`

export const Keywords = styled.ul`
  font-size: 10px;
  font-family: 'arial';
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  margin: 0px;
  padding: 0px;

  li {
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 5px;
    padding: 3px 10px 3px 10px;
    margin: 3px;
    list-style-type: none;
    color: #fff;
    font-weight: bold;
    font-size: 12px;
    background-color: #a3a776;
  }
`

export const Author = styled.div`
  padding: 10px 0px 5px 0px;
  font-size: 12px;
`

export const PublishedDate = styled.div`
  font-size: 12px;
`

export const ImgIcon = styled.img``

export const Options = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;

  svg {
    margin: 5px;
    width: 20px;
    height: 20px;
    cursor: pointer;
  }
`

export const Content = styled.div`
  padding-top: 20px;
  line-height: 30px;
  margin-bottom: 40px;
  color: ${({ theme }) => theme.text};

  h1, h2, h3, h4 {
    color: ${({ theme }) => theme.text};
  }

  p {
    line-height: 30px;
    margin-bottom: 20px;
  }

  img {
    max-width: 95%;
    display: block;
    margin-left: auto;
    margin-right: auto;
  }

  th, td {
    padding: 15px;
    text-align: left;
    border-bottom: 1px solid #ddd;
  }

  /* maytham-ɯɐɥʇʎɐɯ, stackoverflow, https://stackoverflow.com/questions/4000792/how-can-i-style-code-listings-using-css */
  pre {
    /* background: #f4f4f4; */
    background: ${({ theme }) => theme.codeBackground};
    border: 1px solid #ddd;
    border-left: 3px solid #a3a776;
    /* color: #666; */
    color: ${({ theme }) => theme.text};
    page-break-inside: avoid;
    font-family: monospace;
    font-size: 15px;
    line-height: 1.6;
    margin-bottom: 1.6em;
    max-width: 100%;
    overflow: auto;
    padding: 1em 1.5em;
    display: block;
    word-wrap: break-word;
  }

  code {
    /* background: #f4f4f4; */
    background: ${({ theme }) => theme.codeBackground};
  }

  .info {
    margin: 5% 0;
    background-color: #A7C7F1;
    padding: 2%;
    border: 1px solid #95bcee;
    border-radius: 5px;
    color: #000;
    opacity: 80%;
  }

  .success {
    margin: 5% 0;
    background-color: #d9f7be;
    padding: 2%;
    border: 1px solid #b7eb8f;
    border-radius: 5px;
    color: #000;
    opacity: 80%;
  }

  .warning {
    margin: 5% 0;
    background-color: #FAB39E;
    padding: 2%;
    border: 1px solid #F89577;
    border-radius: 5px;
    color: #000;
    opacity: 80%;
  }

  .danger {
    margin: 5% 0;
    background-color: #f4cdd3;
    padding: 2%;
    border: 1px solid #EDABB5;
    border-radius: 5px;
    opacity: 80%;
    color: #000;
  }

  .highlight {
    margin: 5% 0;
    background-color: #E8F8C1;
    padding: 2%;
    border: 1px solid #DEF5A3;
    border-radius: 5px;
    opacity: 80%;
    color: #000;
  }
`