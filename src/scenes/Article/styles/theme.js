export const lightTheme = {
  body: '#fff',
  text: '#363537',
  toggleBorder: '#def2c8',
  toggleBorderOpacity: '30%',
  codeBackground: '#f4f4f4',
  gradient: 'linear-gradient(#39598A, #79D7ED)',
}

export const darkTheme = {
  body: '#414428',
  text: '#FAFAFA',
  toggleBorder: '#585d3b',
  codeBackground: '#31331E',
  toggleBorderOpacity: '100%',
  gradient: 'linear-gradient(#091236, #1E215D)',
}