import React, { useEffect, useState } from 'react'
import parse from 'html-react-parser'
import { fetchArticle } from '../../services/articles'
import { toHTML } from '../../services/formater/toHtml'
import { ThemeProvider } from 'styled-components'
import { useDarkMode } from './styles/useDarkMode'
import { lightTheme, darkTheme } from './styles/theme'
import { ReactComponent as Send } from './icons/send.svg'
import {
  ArticleWrapper,
  ArticleContainer,
  Title,
  Lead,
  Author,
  PublishedDate,
  Content,
  Stripe,
  Keywords,
  Options,
} from './styles'
import { Switch, notification } from 'antd'
import * as moment from 'moment'

const Article = ({ match }) => {
  const [article, setArticle] = useState(null)
  const [theme, toggleTheme] = useDarkMode();
  useEffect(() => {
    const fetchData = async () => {
      try {
        const articleRequest =  await fetchArticle(match.url.split('/')[2])
        setArticle(articleRequest)
      } catch (error) {
        console.error('something bad happened', error);
      }
    }
    fetchData()
  },[match.url])

  const formatDate = date => moment(date).format('MMM DD, YYYY')

  const onSwitchDarkMode = () => {
    toggleTheme()
  }

  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({ message, description })
  }

  const copyToClipboard = (url) => {
    navigator.clipboard.writeText(`https://sespinoza.me/#${url}`)
    openNotificationWithIcon('success', 'Copied', 'Link copied!')
    console.log(`https://sespinoza.me/#${url} copied!`)
  }

  return (
    <div>
      <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
        <Stripe/>
        <ArticleWrapper>
          <ArticleContainer>
            <Title>{article && article.title }</Title>
            <Keywords>{article && article.keywords.map(k => <li key={k}>{k}</li>)}</Keywords>
            <Lead>{article && article.lead}</Lead>
            <Author>{article && `By ${article.author}` }</Author>
            <PublishedDate>
                {
                  article &&
                  `${article.published_at ? formatDate(article.published_at) : 'draft'} · ${article.stats.text}`
                }
            </PublishedDate>
            {
              article &&
                <Options>
                <Switch onChange={() => onSwitchDarkMode()} size="medium" checkedChildren="☀" unCheckedChildren="☾" ></Switch>
                <Send onClick={() => copyToClipboard(match.url)}/>
                </Options>
            }
            <Content>
              {
                article ? parse(toHTML(article.content)) : 'loading...'
              }
            </Content>
          </ArticleContainer>
        </ArticleWrapper>
      <Stripe/>
      </ThemeProvider>
    </div>
  )
}

export default Article
