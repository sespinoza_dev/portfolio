import React from 'react';
import Preview from './Preview'
import Skills from './Skills'

const Home = () => {
  return (
    <div>
      <Preview />
      <Skills />
    </div>
  )
}

export default Home
