import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 20px;
`

export const Container = styled.ul`
  padding: 0;
  margin: 0;
  width: 800px;
`

