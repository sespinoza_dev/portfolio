import styled from 'styled-components'

export const BarListItem = styled.li`
  list-style-type: none;
  margin: 15px;
`

export const Bar = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 16px;
  background: rgba(0, 0, 0, 0.05);
  box-shadow: inset 0 1px 2px rgba(0,0,0, 0.25), 0 1px rgba(255, 255, 255, 0.8);
  height: 24px;
  border-radius: 5px;

  span {
    background: rgba(132,137,88, 0.5);
    border-radius: 5px;
    height: 24px;
    background-size: 100%;
    animation: skill-bar-progress 4s ease;
    width: 100%;
  }

  @keyframes skill-bar-progress {
    0% {
      width: 0%;
    }
  }
`

export const SkillName = styled.div`
  border-radius: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 110px;
  height: 24px;
  color: #fff;
  font-weight: bold;
  background-color: rgba(134,121,102, 0.5);

`

export const SkillPercentage = styled.div`
  color: #666;
  padding: 2px;
`
