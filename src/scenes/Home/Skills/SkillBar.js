import React from 'react'
import {
  BarListItem,
  Bar,
  SkillName,
  SkillPercentage,
} from './styles/skillBar.js'

const SkillBar = (name, width) => {
  const style = {
    width: width
  }
  return (
    <BarListItem key={name}>
      <Bar>
        <span style={style}>
          <SkillName>
            {name}
          </SkillName>
        </span>
        <SkillPercentage>
          {width}
        </SkillPercentage>
      </Bar>
    </BarListItem>
  )
}

export default SkillBar
