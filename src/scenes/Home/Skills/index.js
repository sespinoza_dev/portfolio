import React from 'react'
import SkillBar from './SkillBar'
import {
  Wrapper,
  Container,
} from './styles/index.js'

const Skills = () => {
  const skillList = [
    { name: 'Node.js', width: '75%' },
    { name: 'Javascript', width: '80%' },
    { name: 'Docker', width: '75%' },
    { name: 'Bash', width: '50%' },
    { name: 'Git', width: '80%' },
    { name: 'React', width: '50%' },
    { name: 'CSS', width: '25%' },
  ]
  const renserSkillList = skillList.map(item => SkillBar(item.name, item.width))

  return (
    <Wrapper>
      <Container>
        {renserSkillList}
      </Container>
    </Wrapper>
  )
}

export default Skills
