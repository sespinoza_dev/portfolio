import React from 'react'

import { OptionLink } from './styles/index.js'
import {
  Wrapper,
  Container,
  Hero,
} from './styles/index.js'
import Tree from './img/tree.js'

const Preview = () => {
  return (
    <Wrapper>
      <Container>
        <Hero>
          <h1>Hi, there!</h1>
          <h2>I'm a Software Developer</h2>
          <h3>
            I'm Samuel Espinoza, here you can find out
            about <OptionLink to="/about">my work</OptionLink>,
            read my <OptionLink to="/articles">technical articles</OptionLink>,
            and <OptionLink to="/contact">hire me</OptionLink> ;)
          </h3>
        </Hero >
        <Tree />
      </Container>
    </Wrapper>
  )
}

export default Preview
