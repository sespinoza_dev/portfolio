import styled from 'styled-components'
import { Link } from "react-router-dom"

export const Wrapper = styled.div`
  margin: 30px;
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
`

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media screen and (max-width: 800px) {
    justify-content: center;
    align-items: center;

    svg { display: none; }
  }
`

export const Hero = styled.div`
  max-width: 768px;
  color: rgba(0,0,0,.85);
  opacity: 0.6;

  h1 {
    margin: 20px 0px 0px 20px;
    font-family: 'Times New Roman', Times, serif;
    font-weight: normal;
    font-size: 80px;
    display: flex;

    @media screen and (max-width: 800px) { font-size: 60px; }
  }

  h2 {
    margin: 20px 0px 0px 20px;
    font-family: 'Times New Roman', Times, serif;
    font-weight: normal;
    font-size: 30px;
    display: flex;

    @media screen and (max-width: 800px) { margin: 0px 0px 0px 20px; }
  }

  h3 {
    margin: 20px 0px 0px 20px;
    font-family: 'Times New Roman', Times, serif;
    font-weight: normal;
    font-size: 20px;
  }
`

export const OptionLink = styled(Link)`
  color: rgba(0, 0, 0, 0.85);
  font-weight: bold;

  &:hover{
    text-decoration: underline;
    color: rgba(0, 0, 0, 0.85);
  }
`

