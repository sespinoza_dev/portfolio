import styled from 'styled-components'

export const Content = styled.li`
  margin: 0;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
`
