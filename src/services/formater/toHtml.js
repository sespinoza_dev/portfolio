import * as showdown from 'showdown'
const toHTML = content => {
  const converter = new showdown.Converter();
  converter.setOption('tables', true);
  return converter.makeHtml(content);
}

export {
  toHTML
}