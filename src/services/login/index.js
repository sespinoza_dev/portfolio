import axios from 'axios'

const login = payload => {
  return new Promise((resolve, reject) => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const url = (process.env.NODE_ENV === 'development') ?
      'http://localhost:4004/login/api/v1/users/login/' :
      'https://sespinoza.me/login/api/v1/users/login/'
    axios.post(url, payload, config)
    .then(({ data }) => {
      localStorage.setItem("user", JSON.stringify(data));
      resolve(data)
    })
    .catch(error => {
      if (error.response) {
        console.error(error.response.data);
        console.error(error.response.status);
        console.error(error.response.headers);
      }
      reject(error)
    });
  })
}

const logout = token => {
  return new Promise((resolve, reject) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': token
      }
    }
    const url = (process.env.NODE_ENV === 'development') ?
      'http://localhost:4004/login/api/v1/users/me/logoutall/' :
      'https://sespinoza.me/login/api/v1/users/me/logoutall/'
    axios.post(url, {}, config)
      .then(({ data }) => {
        localStorage.removeItem("user");
        resolve(data)
      })
      .catch(error => {
        if (error.response) {
          console.error(error.response.data);
          console.error(error.response.status);
          console.error(error.response.headers);
        }
        reject(error)
      });
  })
}

export {
  login,
  logout
}