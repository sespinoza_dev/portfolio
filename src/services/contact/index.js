import axios from 'axios'

const sendContactForm = async payload => {
  try {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const url = 'https://sespinoza.me/contact/api/send'
    console.log('payload')
    console.log(payload)
    const { data } = await axios.post(url, payload, config)
    console.log(data);
    return data
  } catch (error) {
    console.error(error)
    throw Error(error)
  }
}

export {
  sendContactForm
}
