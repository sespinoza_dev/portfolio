const axios = require('axios')

const fetchArticles = async () => {
  return new Promise((resolve, reject) => {
    const url = (process.env.NODE_ENV === 'development') ?
      'http://localhost:4003/articles/api/v1':
      'https://sespinoza.me/articles/api/v1'
    return axios.get(url)
      .then(({ data }) => {
        console.log('fetch articles', data)
        resolve(data)
      })
      .catch(error => {
        if (error.response) {
          console.error(error.response.data);
          console.error(error.response.status);
          console.error(error.response.headers);
        }
        reject(error)
      });
  })
}

const fetchAuthorArticles = async (authorId, token) => {
  try {
    const url = (process.env.NODE_ENV === 'development') ?
      `http://localhost:4003/articles/api/v1/author/${authorId}` :
      `https://sespinoza.me/articles/api/v1/author/${authorId}`
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      }
    }
    const { data } = await axios.get(url, config)
    console.log('fetch authors articles', data)
    return data
  } catch (error) {
    throw Error(error)
  }
}

const fetchArticle = async id => {
  try {
    const url = (process.env.NODE_ENV === 'development') ?
      `http://localhost:4003/articles/api/v1/${id}`:
      `https://sespinoza.me/articles/api/v1/${id}`
    const { data } = await axios.get(url)
    console.log('fetch Article', data)
    return data
  } catch (error) {
    console.error(error);
    throw Error(error)
  }
}

const postArticle = async (article, token) => {
  return new Promise((resolve, reject) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      }
    }
    const url = (process.env.NODE_ENV === 'development') ?
      'http://localhost:4003/articles/api/v1/':
      'https://sespinoza.me/articles/api/v1/'
    return axios.post(url, article, config)
      .then(({ data }) => {
        console.log('article created', data)
        resolve(data)
      })
      .catch(error => {
        if (error.response) {
          console.error(error.response.data);
          console.error(error.response.status);
          console.error(error.response.headers);
        }
        reject(error)
      });
  })
}

const updateArticle = async (article, token) => {
  return new Promise((resolve, reject) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      }
    }
    const url = (process.env.NODE_ENV === 'development') ?
      `http://localhost:4003/articles/api/v1/${article._id}`:
      `https://sespinoza.me/articles/api/v1/${article._id}`
    return axios.put(url, article, config)
      .then(({ data }) => {
        console.log('article updated', data)
        resolve(data)
      })
      .catch(error => {
        if (error.response) {
          console.error(error.response.data);
          console.error(error.response.status);
          console.error(error.response.headers);
        }
        reject(error)
      });
  })
}

const publishArticle = async (id, token) => {
  return new Promise((resolve, reject) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      }
    }
    const url = (process.env.NODE_ENV === 'development') ?
      `http://localhost:4003/articles/api/v1//publish/${id}`:
      `https://sespinoza.me/articles/api/v1//publish/${id}`
    console.log(url)
    return axios.patch(url, {}, config)
      .then(({ data }) => {
        console.log('article created', data)
        resolve(data)
      })
      .catch(error => {
        if (error.response) {
          console.error(error.response.data);
          console.error(error.response.status);
          console.error(error.response.headers);
        }
        reject(error)
      });
  })
}

const deleteArticle = async (id, token) => {
  return new Promise((resolve, reject) => {
    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: token
      }
    }
    const url = (process.env.NODE_ENV === 'development') ?
      `http://localhost:4003/articles/api/v1/${id}`:
      `https://sespinoza.me/articles/api/v1/${id}`
    console.log(config)
    return axios.delete(url, config)
      .then(({ data }) => {
        console.log('article deleted', data)
        resolve(data)
      })
      .catch(error => {
        if (error.response) {
          console.error(error.response.data);
          console.error(error.response.status);
          console.error(error.response.headers);
        }
        reject(error)
      });
  })
}

export {
  fetchArticles,
  fetchAuthorArticles,
  fetchArticle,
  postArticle,
  updateArticle,
  deleteArticle,
  publishArticle,
}
