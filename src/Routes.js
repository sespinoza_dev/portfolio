import React from 'react'
import { Routes, Route, Navigate } from 'react-router-dom'

import Home from './scenes/Home'
import Articles from './scenes/Articles'
import WriteArticles from './scenes/WriteArticle'
import Article from './scenes/Article'
import ArticlesAdmin from './scenes/ArticlesAdmin'
import About from './scenes/About'
import Contact from './scenes/Contact'
import Login from './scenes/Login'

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/articles" element={<Articles />} />
      <Route path="/articles/edit/:id" element={<WriteArticles />} />
      <Route path="/articles/admin" element={<ArticlesAdmin />} />
      <Route path="/articles/:id" element={<Article />} />
      <Route path="/contact" element={<Contact />} />
      <Route path="/login" element={<Login />} />
      {/* Redirect route */}
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
};

export default AppRoutes
