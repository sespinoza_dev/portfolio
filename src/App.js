import React from 'react'
import { HashRouter } from 'react-router-dom'
import Header from './Header'
import Footer from './Footer'
import Routes from './Routes'
import { Content } from './AppStyled'
import { useDispatch } from 'react-redux'

const App = () => {
  const userSession = localStorage.getItem("user")
  const dispatch = useDispatch()
  if (userSession) {
    dispatch({
      type: 'USER_DATA',
      payload: JSON.parse(userSession)
    })
  }
  return (
    <Content>
      <HashRouter>
        <Header />
        <Routes/>
        <Footer />
      </HashRouter>
    </Content>
  )
}

export default App
