export const authToken = payload => ({
  type: 'AUTH_TOKEN',
  payload
});