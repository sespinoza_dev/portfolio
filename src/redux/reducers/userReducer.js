const initialState = {}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'USER_DATA':
      return userData(action, state);
  
    default:
      return state;
  }
}

const userData = (action, state) => {
  return action.payload;
}

export default authReducer