import styled from 'styled-components'

export const HeaderWrapper = styled.nav`
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.25);
  display: flex;
  justify-content: space-around;
`

export const UnorderedList = styled.ul`
  margin: 0px;
  padding: 0px;
  width: 100%;

  /* desktop resolution */
  @media screen and (min-width: 768px) {
    display: flex;
    width: auto;
  }
`

export const ListItem = styled.li`
  /* mobile resolution */
  display: ${props => (props.toggle ? `block` : `none`)};
  list-style-type: none;
  color: #7D7D7D;
  font-family: 'Roboto';
  text-align: center;
  font-size: 14px;
  width: 100%;

  .anticon-user {
    display: block;
  }

  span {
    display: block;
    padding: 10px 50px 10px 50px;
    border-bottom: 3px solid #ffffff;
    text-decoration: none;
  }

  div {
    display: block;
    padding: 10px 50px 10px 50px;
    border-bottom: 3px solid #ffffff;
    text-decoration: none;
  }

  div:hover {
    background-color: #F2F2F2;
    border-bottom: 3px solid #a3a776;
  }

  a {
    color: #7D7D7D;
    display: block;
    padding: 10px 50px 10px 50px;
    border-bottom: 3px solid #ffffff;
    text-decoration: none;
  }

  a:visited {
    color: #7D7D7D;
  }

  a:hover {
    color: #7D7D7D;
    background-color: #F2F2F2;
    border-bottom: 3px solid #a3a776;
  }

  // :last-child {
  //   display: none;
  // }

  /* login & logout */
  :nth-child(6) {
    display: ${props => (props.toggle ? `block` : `none`)};;
  }

  /* desktop resolution */
  @media screen and (min-width: 768px) {
    display: block;
    font-size: 15px;

    .anticon-menu .anticon {
      display: block;
    }

    .anticon-user {
      display: block;
    }

    // :last-child {
    //   display: block;
    // }

    /* login & logout */
    :nth-child(6) {
      display: none;
    }

    span {
      display: none;
      padding: 10px 50px 10px 50px;
      border-bottom: 3px solid #ffffff;
      text-decoration: none;
    }
  }
`
