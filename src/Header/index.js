import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom"
import { useSelector } from 'react-redux'
import { notification } from 'antd'
import { logout } from '../services/login'
import { HeaderWrapper, ListItem, UnorderedList } from './styles/index.js'
import { MenuOutlined } from '@ant-design/icons'
import { Menu, Dropdown } from 'antd'

const Header = () => {
  const userData = useSelector(state => state.user)
  const [toggle, setToggle] = useState(false)
  const [email, setEmail] = useState(undefined)

  useEffect(() => {
    const currentEmail = userData.user && userData.user.email;
    setEmail(currentEmail)
  }, [userData.user])

  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({ message, description })
  }

  const handleOnClick = () => {
    setToggle(!toggle)
  }

  const handleLogout = async () => {
    try {
      await logout(userData.token)
      setEmail(undefined)
      setToggle(!toggle)
      return openNotificationWithIcon('success', 'Log out', 'Successfully logged out')
    } catch (error) {
      return openNotificationWithIcon('error', 'Log out', 'Bada boom! cannot log out')
    }
  }

  const menu = (
    <Menu>
      {
        email &&
          <Menu.Item>
            <Link
              onClick={() => handleOnClick()}
              to="/articles/admin">
                 Write a Story
            </Link>
          </Menu.Item>
      }
      {
        email &&
          <Menu.Item> {email && <div>{email.split('@')[0]}</div>}</Menu.Item>
      }
      <Menu.Item>
        {
          email ?
            <Link
              onClick={() => handleLogout()}
              to="/">
                logout
            </Link> :
            <Link
              onClick={() => handleOnClick()}
              to="/login">
                login
              </Link>
        }
      </Menu.Item>
    </Menu>
  );

  return (
    <HeaderWrapper>
      <UnorderedList>
        <ListItem toggle={true}><MenuOutlined onClick={() => handleOnClick()}/></ListItem>
        <ListItem toggle={toggle}><Link onClick={() => handleOnClick()} to="/">Home</Link></ListItem>
        <ListItem toggle={toggle}><Link onClick={() => handleOnClick()} to="/about">About</Link></ListItem>
        <ListItem toggle={toggle}><Link onClick={() => handleOnClick()} to="/articles">Articles</Link></ListItem>
        <ListItem toggle={toggle}><Link onClick={() => handleOnClick()} to="/contact">Contact</Link></ListItem>
        {
          email ?
            <ListItem toggle={toggle}>
              <Link
                onClick={() => handleLogout()}
                to="/">
                  logout
              </Link>
            </ListItem> :
            <ListItem toggle={toggle}>
              <Link
                onClick={() => handleOnClick()}
                to="/login">
                  login
              </Link>
            </ListItem>
        }
        <ListItem toggle={toggle}>
          <Dropdown overlay={menu}>
            <div>User</div>
          </Dropdown>
        </ListItem>
      </UnorderedList>
    </HeaderWrapper>
  )
}

export default Header
