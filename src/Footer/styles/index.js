
import styled from 'styled-components'

export const FooterStyled = styled.footer`
  display: flex;
  margin-top: auto;
  justify-content: space-around;
  color: #ffffff;
  padding: 20px;
  background-color: #898980;
  opacity: 0.8;
`
