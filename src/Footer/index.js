import React from 'react'
import { FooterStyled } from './styles/index.js'

const Footer = () => {
  return (
    <FooterStyled>
      Copyright &copy; 2020 S.Espinoza.
    </FooterStyled>
  )
}

export default Footer;
